//
//  TrashWorker.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 29/01/17.
//  Copyright (c) 2017 Catalystwo. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

class TrashWorker
{
  // MARK: - Business Logic
  
  func deleteAllRecords()
  {
    DataSource.shared.deleteAll()
  }
}

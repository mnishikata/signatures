//
//  TrashInteractor.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 29/01/17.
//  Copyright (c) 2017 Catalystwo. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol TrashInteractorInput
{
  func allowDeletion(request: Trash.Deletion.Request)
}

protocol TrashInteractorOutput
{
  func allowDeletion(response: Trash.Deletion.Response)
}

class TrashInteractor: TrashInteractorInput
{
  var output: TrashInteractorOutput!
  lazy var worker = TrashWorker()
  
  // MARK: - Business logic
  
  func allowDeletion(request: Trash.Deletion.Request) {
    // NOTE: Create some Worker to do the work
    
    // NOTE: Pass the result to the Presenter
    
    let response = Trash.Deletion.Response(isDeletionAllowed: request.allowDeletion)
    output.allowDeletion(response: response)
  }
  
  func doDelete(request: Trash.DeleteCommand.Request) {
    worker.deleteAllRecords()
  }
}

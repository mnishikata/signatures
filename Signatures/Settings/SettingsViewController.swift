//
//  SettingsViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 25/01/17.
//  Copyright (c) 2017 Catalystwo. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol SettingsViewControllerInput
{
  func displayUserDefaults(viewModel: Settings.Get.ViewModel)
}

protocol SettingsViewControllerOutput
{
  func setUserDefaults(request: Settings.Set.Request)
  func userDefaults(request: Settings.Get.Request)
}

class SettingsViewController: UIViewController, SettingsViewControllerInput
{
  var output: SettingsViewControllerOutput!
  var router: SettingsRouter!
  
  @IBOutlet weak var emailField: UITextField!
  @IBOutlet weak var sortSwitch: UISwitch!

  
  // MARK: - Object lifecycle
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    SettingsConfigurator.sharedInstance.configure(viewController: self)
  }
  
  // MARK: - View lifecycle
  
  @IBAction func sortChanged(_ sender: Any) {
    output.setUserDefaults(request: Settings.Set.Request(key: UserDefaultsStorage.descendingSortOrder, value: !sortSwitch.isOn))
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    output.userDefaults(request: Settings.Get.Request(types: [UserDefaultsStorage.defaultEmail, UserDefaultsStorage.descendingSortOrder]))
  }
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    output.setUserDefaults(request: Settings.Set.Request(key: UserDefaultsStorage.defaultEmail, value: emailField.text))
  }
  
  
  // MARK: - Display logic
  
  func displayUserDefaults(viewModel: Settings.Get.ViewModel) {
    // NOTE: Display the result from the Presenter
    let dictionary = viewModel.dictionary
    for key in dictionary.keys {
      switch key {
      case UserDefaultsStorage.defaultEmail:
        emailField.text = dictionary[key] as! String?
        
      case UserDefaultsStorage.descendingSortOrder:
        sortSwitch.isOn = !(dictionary[key] as! Bool)
        
      default: break
      }
    }
  }
  
}

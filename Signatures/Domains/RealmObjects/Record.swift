//
//  Record.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation
import RealmSwift

final class Record: Object {
  dynamic var uuid: String!
  dynamic var signature: Data!
  dynamic var printName: Data!
  dynamic var checked: Bool = false
  dynamic var creationDate: NSDate!
  dynamic var timestamp: NSDate!
  dynamic var isDeleted: Bool = false

  override static func indexedProperties() -> [String] {
    return ["timestamp", "isDeleted", "creationDate", "checked"]
  }
  
  override static func primaryKey() -> String? {
    return "uuid"
  }
}

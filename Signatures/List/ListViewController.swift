//
//  ListViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 29/01/17.
//  Copyright (c) 2017 Catalystwo. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit
import RealmSwift
import Messages
import MessageUI

protocol ListViewControllerInput
{
  func displayMailComposer(viewModel: List.SendPDF.ViewModel)
  func displayFetch(viewModel: List.Fetch.ViewModel)
}

protocol ListViewControllerOutput
{
  func sendPDF(request: List.SendPDF.Request)
  func fetch(request: List.Fetch.Request)
  func command(request: List.Command.Request)
  var uncheckedImage: UIImage { get }
  var checkedImage: UIImage { get }
  func dateString(from date: Date) -> String
}

class ListViewController: UITableViewController, ListViewControllerInput
{
  var output: ListViewControllerOutput!
  var router: ListRouter!
  
  var records: Results<Record>!
  
  // MARK: - Object lifecycle
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    ListConfigurator.sharedInstance.configure(viewController: self)
  }
  
  // MARK: - View lifecycle
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    fetchOnLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    self.navigationController?.isToolbarHidden = false
    
    NotificationCenter.default.addObserver(self, selector: #selector(didUpdateSettings), name: DataSourceNotification.setNeedsDisplay, object: nil)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.removeObserver(self, name: DataSourceNotification.setNeedsDisplay, object: nil)
  }
  
  // MARK: - Event handling
  
  func fetchOnLoad()
  {
    // NOTE: Ask the Interactor to do some work
    
    let request = List.Fetch.Request()
    output.fetch(request: request)
  }
  
  // MARK: - Display logic
  
  func displayFetch(viewModel: List.Fetch.ViewModel)
  {
    self.records = viewModel.records
    reload()
  }
  
  func displayMailComposer(viewModel: List.SendPDF.ViewModel)
  {
    let controller = MFMailComposeViewController()
    controller.modalPresentationStyle = .formSheet
    controller.addAttachmentData(viewModel.pdfData, mimeType: viewModel.mimeType, fileName: viewModel.fileName)
    controller.setToRecipients(viewModel.recipients)
    controller.mailComposeDelegate = self
    present(controller, animated: true, completion: nil)
  }
  
  @IBAction func add() {
    router.add()
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let records = records else { return 0 }
    return records.count
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 75
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecordCell
    
    guard records.count > indexPath.row else { return cell }
    let record: Record = records[indexPath.row]
    cell.timestampLabel.text = output.dateString(from: record.timestamp as Date)
    
    if let button = cell.checkButton {
      button.action = { [unowned button] in
        button.isSelected = !button.isSelected
        
        let request = List.Command.Request(command: List.CommandType.update, record: self.records[indexPath.row], checked: button.isSelected)
        self.output.command(request: request)
      }
      
      button.tag = indexPath.row
      button.setTitle(nil, for: .normal)
      button.setImage(output.uncheckedImage, for: .normal)
      button.setImage(output.checkedImage, for: .selected)
      button.isSelected = record.checked
    }
    
    if let tanzaku = MNTanzaku(from: record.printName) {
      cell.imageView1.tanzaku = tanzaku
    }
    
    if let tanzaku = MNTanzaku(from: record.signature) {
      cell.imageView2.tanzaku = tanzaku
    }
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    guard records.count > indexPath.row else { return }
    
    let request = List.Command.Request(command: List.CommandType.delete, record: records[indexPath.row], checked:nil)
    self.output.command(request: request)

    tableView.deleteRows(at: [indexPath], with: .automatic)
  }
  
  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func didUpdateSettings() {
    let request = List.Fetch.Request()
    output.fetch(request: request)
  }
  
  func reload() {
    tableView.reloadData()
  }
  
  @IBAction func edit(_ sender: Any) {
    self.setEditing(!self.isEditing, animated: true)
  }
  
  @IBAction func export(_ sender: Any) {
    guard MFMailComposeViewController.canSendMail() == true else { return }
    output.sendPDF(request: List.SendPDF.Request())
  }
}


extension ListViewController: MFMailComposeViewControllerDelegate {
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    
    controller.dismiss(animated: true, completion: nil)
  }
}

extension ListViewController: GLPadViewControllerDelegate {
  
  func padViewControllerWillClose(_ record: Record) {
    
    let request = List.Command.Request(command: List.CommandType.add, record: record, checked:nil)
    self.output.command(request: request)

    reload()
  }
}

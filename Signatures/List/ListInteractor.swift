//
//  ListInteractor.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 29/01/17.
//  Copyright (c) 2017 Catalystwo. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol ListInteractorInput
{
  func sendPDF(request: List.SendPDF.Request)
  func fetch(request: List.Fetch.Request)
  func command(request: List.Command.Request)
  var uncheckedImage: UIImage { get }
  var checkedImage: UIImage { get }
  func dateString(from date: Date) -> String
}

protocol ListInteractorOutput
{
  func presentMailComposer(response: List.SendPDF.Response)
  func presentFetch(response: List.Fetch.Response)
}

class ListInteractor: ListInteractorInput
{
  var output: ListInteractorOutput!
  lazy var worker = ListWorker()
  
  var uncheckedImage: UIImage {
    return worker.checkImage(false)
  }
  var checkedImage: UIImage {
    return worker.checkImage(true)
  }
  func dateString(from date: Date) -> String {
    return worker.dateString(from: date)
  }

  // MARK: - Business logic
  
  func sendPDF(request: List.SendPDF.Request)
  {
    // NOTE: Create some Worker to do the work
    let records = DataSource.shared.allRecords

    guard let attachmentUrl = worker.pdf(from: records) else { return }
    guard let data = try? Data(contentsOf: attachmentUrl) else { return }

    // NOTE: Pass the result to the Presenter
    
    let recipients = UserDefaultsStorage().defaultEmail?.components(separatedBy: ",")
    let response = List.SendPDF.Response(pdfData: data, recipients: recipients)

    output.presentMailComposer(response: response)
  }
  
  func fetch(request: List.Fetch.Request) {
    let records = DataSource.shared.allRecords

    let response = List.Fetch.Response(records: records)
    output.presentFetch(response: response)
  }
  
  func command(request: List.Command.Request) {

    switch request.command {
    case .add:
      DataSource.shared.addRecord(request.record)
    case .delete:
      DataSource.shared.deleteRecord(request.record)
    case .update:
      if let checked = request.checked {
        DataSource.shared.updateRecord(request.record, checked: checked)
      }
    }

  }
}
